# Spring Analysis 
## 项目介绍
- 本项目为 Spring 源码分析项目

[![Netlify Status](https://api.netlify.com/api/v1/badges/7d4d612b-0d73-47e7-a4bf-2c8e9da26bbb/deploy-status)](https://app.netlify.com/sites/huifer-spring-analysis/deploys)
- [Netlify]: https://huifer-spring-analysis.netlify.app/
## 文章
- bean
    - [propertyEditor: 属性编辑器](./docs/beans/propertyEditor/Readme.md)
    - [Scope: 作用域接口](/docs/beans/Scope/Readme.md)
    - [BeanMetadataElement: bean 元数据接口](/docs/beans/BeanMetadataElement/Readme.md)
    - [BeanDefinition: bean 定义接口](/docs/beans/BeanDefinition/Readme.md)
    - [BeanReference: bean 连接接口](/docs/beans/BeanMetadataElement/BeanReference/Spring-BeanReference.md)
    - [Mergeable: 合并接口](/docs/beans/BeanMetadataElement/Mergeable/Readme.md)
    - [ComponentDefinition: 组件定义接口](/docs/beans/ComponentDefinition/Readme.md)
- core
    - [Convert: 转接接口相关](/docs/core/convert/Readme.md)
- env
    - [PropertyResolver: 属性解析](/docs/env/PropertyResolver/Readme.md)
- utils
    - [StringValueResolver: 字符串值解析工具](/docs/utils/StringValueResolver/Readme.md)